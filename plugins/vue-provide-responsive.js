import Vue from 'vue'
import VueResponsiveProvide from 'vue-provide-responsive'

Vue.use(VueResponsiveProvide, {
	breakpoints: {
		mobile: 767,
		tablet: 1017
	}
})
